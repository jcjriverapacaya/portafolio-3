const BtnToggler = document.getElementById('BtnToggler'),
  BtnClose = document.getElementById('BtnClose'),
  Nav = document.getElementById('Nav'),
  GridContainer = document.getElementById('GridContainer'),
  ContainerContent = document.getElementById('ContainerContent'),
  Body = document.getElementById('Body')


const ActiveMenu = () => {
  Nav.classList.toggle('is-show')
  GridContainer.classList.toggle('is-hidden')
  Body.classList.toggle('is-block')
}
const CloseMenu = () => {
  Nav.classList.remove('is-show')
  GridContainer.classList.remove('is-hidden')
  Body.classList.remove('is-block')
}

BtnToggler.addEventListener('click', ActiveMenu)
BtnClose.addEventListener('click', CloseMenu)
ContainerContent.addEventListener('click',CloseMenu)

